import { useEffect, useState, useCallback } from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { Typography } from "@mui/material";

import ArtList from "../ArtList/ArtList";
import { getFavouriteArts } from "../../resources/arts";

const FavouriteArts = () => {
  const [arts, setArts] = useState([]);
  const favouriteIds = useSelector((state) => state.favourites.favouriteIds);

  const loadData = useCallback(async () => {
    const { data } = await getFavouriteArts(favouriteIds);
    setArts(data);
  }, [favouriteIds]);

  useEffect(() => {
    loadData();
  }, [loadData]);

  return (
    <div className="arts-list">
      <div className="link-wrapper">
        <Link className="link" to="/">
          Back
        </Link>
      </div>
      <div className="favourite-arts">
        {favouriteIds.length > 0 ? (
          <ArtList arts={arts} />
        ) : (
          <Typography variant="h4" component="h4">
            There is not any favourite art!
          </Typography>
        )}
      </div>
    </div>
  );
};

export default FavouriteArts;
