import { IconButton, Tooltip } from "@mui/material";
import StarOutlineIcon from "@mui/icons-material/StarOutline";
import GradeIcon from "@mui/icons-material/Grade";

const FavouriteHandler = ({
  isFavourite,
  onAddFavouriteId,
  onRemoveFavouriteId,
}) => {
  return isFavourite ? (
    <Tooltip title="Remove from favourites">
      <IconButton data-testid="remove" onClick={onRemoveFavouriteId}>
        <GradeIcon />
      </IconButton>
    </Tooltip>
  ) : (
    <Tooltip title="Add to favourites">
      <IconButton data-testid="add" onClick={onAddFavouriteId}>
        <StarOutlineIcon />
      </IconButton>
    </Tooltip>
  );
};

export default FavouriteHandler;
