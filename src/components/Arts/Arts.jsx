import { useEffect, useState, useRef, useCallback } from "react";
import { Link } from "react-router-dom";
import { Pagination, OutlinedInput, Paper, IconButton } from "@mui/material";
import { debounce } from "lodash";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import ArtList from "../ArtList/ArtList";
import { getArts, searchArts } from "../../resources/arts";

const Arts = () => {
  const [arts, setArts] = useState([]);
  const [totalPages, setTotalPages] = useState(1);
  const [page, setPage] = useState(1);
  const [isSearch, setIsSearch] = useState(false);
  const inputRef = useRef(null);

  const loadData = useCallback(async () => {
    const { data, pagination } = await getArts(page);
    setTotalPages(pagination.total_pages);
    setArts(data);
  }, [page]);

  useEffect(() => {
    loadData();
  }, [page, loadData]);

  const handlePageChange = (_, value) => {
    setPage(value);
  };

  const handleChange = async () => {
    const { data } = await searchArts(inputRef.current.value);
    setArts(data);
    setIsSearch(true);
  };

  const handleCloseSearch = () => {
    inputRef.current.value = "";
    loadData();
    setIsSearch(false);
  };

  return (
    <div className="arts-list">
      <Paper className="arts-header">
        <div>
          <OutlinedInput
            placeholder="Search"
            inputProps={{ "aria-label": "search", ref: inputRef }}
            className="input"
            variant="outlined"
            onChange={debounce(handleChange, 600)}
          />
          {isSearch && (
            <IconButton aria-label="close" onClick={handleCloseSearch}>
              <HighlightOffIcon />
            </IconButton>
          )}
        </div>
        <div className="link-wrapper">
          <Link className="link" to="/favourites">
            Favourites
          </Link>
        </div>
      </Paper>
      <ArtList arts={arts} />
      {!isSearch && (
        <div className="pagination">
          <Pagination
            count={totalPages}
            page={page}
            onChange={handlePageChange}
          />
        </div>
      )}
    </div>
  );
};

export default Arts;
