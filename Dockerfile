FROM node:14-alpine

WORKDIR /artapp

COPY package*.json ./

RUN npm install

COPY . /artapp

RUN npm run build

ENV NODE_ENV production

EXPOSE 3000

CMD ["npm", "start"]