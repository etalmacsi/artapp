import { ADD_FAVOURITE, REMOVE_FAVOURITE } from "../types/favouritesTypes";

export const addFavourite = (id) => ({
  type: ADD_FAVOURITE,
  payload: id,
});

export const removeFavourite = (id) => ({
  type: REMOVE_FAVOURITE,
  payload: id,
});
