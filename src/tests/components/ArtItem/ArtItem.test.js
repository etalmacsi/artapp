import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import { useNavigate } from "react-router-dom";
import ArtItem from "../../../components/ArtItem/ArtItem";

jest.mock("react-router-dom", () => ({
  useNavigate: jest.fn(),
}));

describe("ArtItem", () => {
  const art = {
    id: 1,
    title: "Mona Lisa",
    image_id: "123",
    thumbnail: { alt_text: "Test" },
  };
  const isFavourite = true;
  const onAddFavouriteId = jest.fn();
  const onRemoveFavouriteId = jest.fn();

  beforeEach(() => {
    useNavigate.mockReset();
  });

  it("should navigate to the correct URL when Open button is clicked", () => {
    const navigateMock = jest.fn();
    useNavigate.mockReturnValue(navigateMock);

    render(
      <ArtItem
        art={art}
        isFavourite={isFavourite}
        onAddFavouriteId={onAddFavouriteId}
        onRemoveFavouriteId={onRemoveFavouriteId}
      />
    );

    fireEvent.click(screen.getByLabelText("add"));

    expect(navigateMock).toHaveBeenCalledWith(`/art/${art.id}`);
  });

  it("should call onRemoveFavouriteId when remove is clicked", () => {
    render(
      <ArtItem
        art={art}
        isFavourite={isFavourite}
        onAddFavouriteId={onAddFavouriteId}
        onRemoveFavouriteId={onRemoveFavouriteId}
      />
    );

    fireEvent.click(screen.getByTestId("remove"));

    expect(onRemoveFavouriteId).toHaveBeenCalledWith(art.id);
  });

  it("should call onAddFavouriteId when add is clicked", () => {
    render(
      <ArtItem
        art={art}
        isFavourite={!isFavourite}
        onAddFavouriteId={onAddFavouriteId}
        onRemoveFavouriteId={onRemoveFavouriteId}
      />
    );

    fireEvent.click(screen.getByTestId("add"));

    expect(onAddFavouriteId).toHaveBeenCalledWith(art.id);
  });

  it("should render the correct title", () => {
    render(
      <ArtItem
        art={art}
        isFavourite={isFavourite}
        onAddFavouriteId={onAddFavouriteId}
        onRemoveFavouriteId={onRemoveFavouriteId}
      />
    );

    expect(screen.getByText(art.title)).toBeInTheDocument();
  });
});
