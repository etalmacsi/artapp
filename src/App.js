import { BrowserRouter, Routes, Route } from "react-router-dom";
import Art from "./components/Art/Art";
import Arts from "./components/Arts/Arts";
import FavouriteArts from "./components/FavouriteArts/FavouriteArts";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Arts />} />
        <Route path="/art/:id" element={<Art />} />
        <Route path="/favourites" element={<FavouriteArts />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
