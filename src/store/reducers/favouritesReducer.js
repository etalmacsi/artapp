import { ADD_FAVOURITE, REMOVE_FAVOURITE } from "../types/favouritesTypes";

const initialState = { favouriteIds: [] };

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_FAVOURITE: {
      return { favouriteIds: [...state.favouriteIds, action.payload] };
    }
    case REMOVE_FAVOURITE: {
      const newIds = state.favouriteIds.filter((id) => id !== action.payload);
      return { favouriteIds: newIds };
    }
    default:
      return state;
  }
};

export default reducer;
