import { render, screen } from "@testing-library/react";
import { useSelector, useDispatch } from "react-redux";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ArtList from "../../../components/ArtList/ArtList";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

describe("ArtList", () => {
  const mockArt = [
    { id: 1, title: "Art 1", artist: "Artist 1" },
    { id: 2, title: "Art 2", artist: "Artist 2" },
    { id: 3, title: "Art 3", artist: "Artist 3" },
  ];

  beforeEach(() => {
    useSelector.mockImplementation((callback) =>
      callback({
        favourites: { favouriteIds: [] },
      })
    );

    useDispatch.mockReturnValue(jest.fn());
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("should render correctly with arts", () => {
    const container = document.createElement("div");

    render(
      <BrowserRouter>
        <Routes>
          <Route path="*" element={<ArtList arts={mockArt} />} />
        </Routes>
      </BrowserRouter>,
      container
    );

    expect(screen.getByText(mockArt[0].title)).toBeInTheDocument();
    expect(screen.getByText(mockArt[1].title)).toBeInTheDocument();
    expect(screen.getByText(mockArt[2].title)).toBeInTheDocument();
  });
});
