import { configureStore } from "@reduxjs/toolkit";
import favouritesReducer from "./reducers/favouritesReducer";

export default configureStore({
  reducer: {
    favourites: favouritesReducer,
  },
});
