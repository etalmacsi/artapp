import api from "./index.js";

const PAGE_LIMIT = 25;

const getArts = async (page, value) => {
  const { data } = await api.get("/artworks", {
    params: {
      limit: PAGE_LIMIT,
      page: page,
      fields: ["id", "thumbnail", "image_id", "title"],
      q: value,
    },
  });
  return data;
};

const getFavouriteArts = async (ids) => {
  const { data } = await api.get("/artworks", {
    params: {
      ids,
      fields: ["id", "thumbnail", "image_id", "title"],
    },
  });
  return data;
};

const searchArts = async (value) => {
  const { data } = await api.get("/artworks/search", {
    params: {
      fields: ["id", "thumbnail", "image_id", "title"],
      q: value,
    },
  });
  return data;
};

const getArt = async (id) => {
  const { data } = await api.get(`/artworks/${id}`, {
    params: {
      fields: [
        "id",
        "thumbnail",
        "image_id",
        "title",
        "artist_title",
        "place_of_origin",
        "fiscal_year",
        "department_title",
        "is_on_view",
        "gallery_title",
        "artwork_type_title",
        "style_titles",
      ],
    },
  });
  return data;
};

export { getArts, searchArts, getArt, getFavouriteArts };
