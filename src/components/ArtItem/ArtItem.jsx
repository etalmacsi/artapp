import { useNavigate } from "react-router-dom";
import { Paper, IconButton, Tooltip } from "@mui/material";
import OpenInNewIcon from "@mui/icons-material/OpenInNew";
import FavouriteHandler from "../FavouriteHandler/FavouriteHandler";

const ArtItem = ({
  art,
  isFavourite,
  onAddFavouriteId,
  onRemoveFavouriteId,
}) => {
  const navigate = useNavigate();

  const handleNavigate = () => {
    navigate(`/art/${art.id}`);
  };

  return (
    <Paper className="art-item">
      {art.image_id && (
        <img
          className="img"
          src={`https://www.artic.edu/iiif/2/${art.image_id}/full/843,/0/default.jpg`}
          alt={art.thumbnail.alt_text}
        />
      )}
      <div className="icons">
        <FavouriteHandler
          isFavourite={isFavourite}
          onAddFavouriteId={() => onAddFavouriteId(art.id)}
          onRemoveFavouriteId={() => onRemoveFavouriteId(art.id)}
        />
        <Tooltip title="Open">
          <IconButton aria-label="add" onClick={handleNavigate}>
            <OpenInNewIcon />
          </IconButton>
        </Tooltip>
      </div>
      <span>{art.title}</span>
    </Paper>
  );
};

export default ArtItem;
