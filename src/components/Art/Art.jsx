import { useEffect, useState, useCallback } from "react";
import { useParams, Link } from "react-router-dom";
import { Paper, Typography } from "@mui/material";
import Divider from "@mui/joy/Divider";
import { useSelector, useDispatch } from "react-redux";
import {
  addFavourite,
  removeFavourite,
} from "../../store/actions/favouritesActions";
import { getArt } from "../../resources/arts";
import FavouriteHandler from "../FavouriteHandler/FavouriteHandler";

const Art = () => {
  const [art, setArt] = useState(null);
  const { id } = useParams();
  const favouriteIds = useSelector((state) => state.favourites.favouriteIds);
  const dispatch = useDispatch();

  const loadData = useCallback(async () => {
    const { data } = await getArt(id);
    setArt(data);
  }, [id]);

  useEffect(() => {
    loadData();
  }, [loadData]);

  const getIsFavourite = (id) => {
    return favouriteIds.includes(id);
  };

  const handleAddFavouriteId = (id) => {
    dispatch(addFavourite(id));
  };

  const handleRemoveFavouriteId = (id) => {
    dispatch(removeFavourite(id));
  };

  return (
    <div className="art-wrapper">
      <div className="art-header">
        <Link className="link" to="/">
          Back
        </Link>
        {art && (
          <FavouriteHandler
            isFavourite={getIsFavourite(art.id)}
            onAddFavouriteId={() => handleAddFavouriteId(art.id)}
            onRemoveFavouriteId={() => handleRemoveFavouriteId(art.id)}
          />
        )}
      </div>

      {art && (
        <Paper className="art">
          <div className="image-wrapper">
            {art.image_id && (
              <img
                className="image"
                src={`https://www.artic.edu/iiif/2/${art.image_id}/full/843,/0/default.jpg`}
                alt={art.thumbnail.alt_text}
              />
            )}
          </div>
          <div className="details-wrapper">
            <Typography variant="h4" component="h4">
              {art.artist_title} - {art.title} ({art.fiscal_year})
            </Typography>
            <Divider className="detail-header" />
            <Typography variant="h6" component="h6">
              Department: {art.department_title}
            </Typography>
            <Divider className="divider" />
            <Typography variant="h6" component="h6">
              Is on view: {art.is_on_view ? "Yes" : "No"}
            </Typography>
            <Divider className="divider" />
            <Typography variant="h6" component="h6">
              Gallery: {art.gallery_title}
            </Typography>
            <Divider className="divider" />
            <Typography variant="h6" component="h6">
              Artwork Type: {art.artwork_type_title}
            </Typography>
            <Divider className="divider" />
            <Typography variant="h6" component="h6">
              Style: {art.style_titles.map((style) => style).join(", ")}
            </Typography>
            <Divider />
          </div>
        </Paper>
      )}
    </div>
  );
};

export default Art;
