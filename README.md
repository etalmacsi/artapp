# Development

For start the application you need a .env file with:

REACT_APP_API_URL="https://api.artic.edu/api/v1"

# For docker

# build

docker build -t art-app .

# start

docker run -p 3000:3000 art-app
