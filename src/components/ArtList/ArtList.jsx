import { useSelector, useDispatch } from "react-redux";
import {
  addFavourite,
  removeFavourite,
} from "../../store/actions/favouritesActions";
import ArtItem from "../ArtItem/ArtItem";

const ArtList = ({ arts }) => {
  const favouriteIds = useSelector((state) => state.favourites.favouriteIds);
  const dispatch = useDispatch();

  const getIsFavourite = (id) => {
    return favouriteIds.includes(id);
  };

  const handleAddFavouriteId = (id) => {
    dispatch(addFavourite(id));
  };

  const handleRemoveFavouriteId = (id) => {
    dispatch(removeFavourite(id));
  };

  return (
    <div className="arts-grid">
      {arts.map((art, index) => (
        <ArtItem
          key={index}
          art={art}
          isFavourite={getIsFavourite(art.id)}
          onAddFavouriteId={handleAddFavouriteId}
          onRemoveFavouriteId={handleRemoveFavouriteId}
        />
      ))}
    </div>
  );
};

export default ArtList;
